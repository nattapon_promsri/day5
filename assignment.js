async function loadAllMovie() {

    let response = await fetch('https://dv-excercise-backend.appspot.com/movies')
    let data = await response.json()
    return data

}

function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')

    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')


    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)


    data.then((json) => {

        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope', 'row')
            dataFirstColumnNode.innerText = currentData['name']
            dataRow.appendChild(dataFirstColumnNode)

            var columnNode = null
            columnNode = document.createElement('td')
            columnNode.innerText = currentData['synopsis']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)

        }
    })
}
async function loadOneMovie() {
    let movie = document.getElementById('movieName').value

    if (movie != '' && movie != null) {
        let response = await fetch('https://dv-excercise-backend.appspot.com/movies/' + movie)
        let data = await response.json()
        return data
    }
}

function createResultTable2(data) {


    let data1 = document.getElementById('showData')

    let wrapper = document.createElement('div')
    wrapper.setAttribute('id', 'wrapper')

    let removeResult = document.getElementById('wrapper')
    data1.appendChild(wrapper)

    if (removeResult != null) {
        data1.removeChild(removeResult)
    }

    data.then((movieList) => {
        for (var i = 0; i <= movieList.length; i++) {

            let movie = movieList[i]

            let movieImage = document.createElement('img')
            movieImage.setAttribute('src', movie['imageUrl'])

            let movieName = document.createElement('p')
            movieName.innerHTML = movie['name']

            let movieDes = document.createElement('p')
            movieDes.innerHTML = movie['synopsis']

            wrapper.appendChild(movieImage)
            wrapper.appendChild(movieName)
            wrapper.appendChild(movieDes)

        }
    })
}
